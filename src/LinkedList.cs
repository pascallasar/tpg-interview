namespace TPG;

/// <summary>
/// A very, very rudimentary implementation of a linked list.
/// For brevity, it contains only the necessary Methods and Member for the task at hand.
/// </summary>
public sealed class LinkedList 
{
    public Node? Last { get; private set; }

    /// <summary>
    /// Adds the data as node to our LinkedList and link it to the last node, if there is one.
    /// </summary>
    /// <param name="data">The data to add to our collection.</param>
    public void Add(int data) {

        // Create a new node with the desired data.
        var node = new Node(data);
        
        // Should it not be the first entry in our list, we link the new node to the last element of our list.
        if(Last != null) {
            node.Next = Last;
        }

        // Set the last node to current node.
        Last = node;
    }

    /// <summary>
    /// Detects a loop in linked nodes. Uses a HashSet to save visited nodes and checks for already containing elements.
    /// 
    /// Executes with O(N) Time Complexity and O(N) Auxiliary Space, where N is the space required to store the visited nodes inside the HashSet.
    /// </summary>
    /// <returns>True if a loop is detected; otherwise false</returns>
    public bool HasLoop() {

        // Create a HashSet to store already visited nodes
        var nodes = new HashSet<Node>();

        // Set our current element to the last element of the LinkedList
        var current = Last;

        // While the next node is not null...
        while(current?.Next != null) {

            //...check if we already visited the node and return true if we have!
            if(nodes.Contains(current))
                return true;
            
            // Add the node to our visited nodes set
            nodes.Add(current);

            // Set the current element to the next element
            current = current.Next;
        }

        // If we reach this point, there is no loop in our LinkedList.
        return false;
    }

    /// <summary>
    /// Checks if the linked nodes contain a loop in a optimized way.
    /// The example with lapped formula one cars was very helpful, thanks! 
    /// 
    /// After I implemented this solution I found - with some research of course - that this method is called: 
    /// Floyd's Cycle-Finding Algorythm.
    /// 
    /// It executes with O(N) Time complexity and O(1) Auxiliary Space. Much more memory efficient then the HashSet implementation.
    /// </summary>
    /// <returns>True if a loop is detected; otherwise false</returns>
    public bool HasLoopOptimized() {

        // Initialize the slow and fast "car" with our start node
        var slow = Last;
        var fast = Last;

        // While slow, fast and the next node of fast is not null...
        while (slow != null && fast != null && fast.Next != null) {
            // "Move" the slow "car" to the next node
            slow = slow.Next;
            // "Move" the fast "car" to the node after the next node.
            fast = fast.Next.Next;

            // Should the cars be equal, we got a loop! So return true.
            if(slow == fast)
                return true;
        }

        // If we reach this point, we can be sure there is no loop in our LinkedList.
        return false;
    }
}
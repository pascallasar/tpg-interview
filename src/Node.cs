namespace TPG;

public sealed class Node
{
    public int Data { get; set; }
    public Node? Next { get; set; }

    public Node(int data)
    {
        Data = data;
    }
}
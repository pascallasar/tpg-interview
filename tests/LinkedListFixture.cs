namespace TPG.Tests;

/// <summary>
/// Simple ClassFixture for the loop detection tests of the LinkedList.
/// </summary>
public class LinkedListFixture 
{
    public LinkedListFixture()
    {
        // Create an empty list to assure it doesn't throw something unexpected or falsely detection of a loop.
        EmptyLinkedList = new LinkedList();

        // Create a valid linked list without a loop.
        NoLoopLinkedList = new LinkedList();
        NoLoopLinkedList.Add(1);
        NoLoopLinkedList.Add(2);
        NoLoopLinkedList.Add(3);
        NoLoopLinkedList.Add(4);
        NoLoopLinkedList.Add(5);

        // Create a closed loop for detection.
        ClosedLoopLinkedList = new LinkedList();
        ClosedLoopLinkedList.Add(1);
        ClosedLoopLinkedList.Add(2);
        ClosedLoopLinkedList.Add(3);
        ClosedLoopLinkedList.Add(4);
        ClosedLoopLinkedList.Add(5);
        ClosedLoopLinkedList.Last!.Next!.Next!.Next!.Next!.Next = ClosedLoopLinkedList.Last;

        // Create a lasso loop for detection
        LassoLoopLinkedList = new LinkedList();
        LassoLoopLinkedList.Add(1);
        LassoLoopLinkedList.Add(2);
        LassoLoopLinkedList.Add(3);
        LassoLoopLinkedList.Add(4);
        LassoLoopLinkedList.Add(5);
        LassoLoopLinkedList.Last!.Next!.Next = LassoLoopLinkedList.Last;
    }

    public LinkedList EmptyLinkedList { get; private set; }
    public LinkedList NoLoopLinkedList { get; private set; }
    public LinkedList ClosedLoopLinkedList { get; private set; }
    public LinkedList LassoLoopLinkedList { get; private set; }
}
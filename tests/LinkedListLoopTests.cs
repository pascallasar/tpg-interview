namespace TPG.Tests;

public class LinkedListLoopTests : IClassFixture<LinkedListFixture>
{
    private readonly LinkedListFixture _fixture;

    public LinkedListLoopTests(LinkedListFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact]
    public void HasLoop_Detects_Loop()
    {
        Assert.True(_fixture.ClosedLoopLinkedList.HasLoop());
    }

    [Fact]
    public void HasLoop_Detects_Lasso_Loop() 
    {
        Assert.True(_fixture.LassoLoopLinkedList.HasLoop());
    }

    [Fact]
    public void HasLoop_Detects_Valid_List() 
    {
        Assert.False(_fixture.NoLoopLinkedList.HasLoop());
    }

    [Fact]
    public void HasLoop_Detects_No_Loop_When_Empty() 
    {
        Assert.False(_fixture.EmptyLinkedList.HasLoop());
    }


       [Fact]
    public void HasLoop_Optimized_Detects_Loop()
    {
        Assert.True(_fixture.ClosedLoopLinkedList.HasLoopOptimized());
    }

    [Fact]
    public void HasLoop_Optimized_Detects_Lasso_Loop() 
    {
        Assert.True(_fixture.LassoLoopLinkedList.HasLoopOptimized());
    }

    [Fact]
    public void HasLoop_Optimized_Detects_Valid_List() 
    {
        Assert.False(_fixture.NoLoopLinkedList.HasLoopOptimized());
    }

    [Fact]
    public void HasLoop_Optimized_Detects_No_Loop_When_Empty() 
    {
        Assert.False(_fixture.EmptyLinkedList.HasLoopOptimized());
    }
}
